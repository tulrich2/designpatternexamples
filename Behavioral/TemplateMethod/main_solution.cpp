#include <iostream>
#include <vector>


class PerpendicularChecker {
public:
  bool check(const std::vector<double> &vec_1, const std::vector<double> &vec_2) {
    auto dot = getDotProduct(vec_1, vec_2);
    
    if (dot == 0) {
      return true;
    } else {
      return false;
    }
  }
  
  
protected:
  virtual double getDotProduct(const std::vector<double> &vec_1, const std::vector<double> &vec_2) = 0;
};


class PerpendicularChecker2D : public PerpendicularChecker {
protected:
  double getDotProduct(const std::vector<double> &vec_1, const std::vector<double> &vec_2) override {
    return vec_1.at(0) * vec_2.at(0) + vec_1.at(1) * vec_2.at(1);
  }
};

class PerpendicularChecker3D : public PerpendicularChecker {
protected:
  double getDotProduct(const std::vector<double> &vec_1, const std::vector<double> &vec_2) override {
    return vec_1.at(0) * vec_2.at(0) + vec_1.at(1) * vec_2.at(1) + vec_1.at(2) * vec_2.at(2);
  }
};


int main(int argc, char **argv) {
  std::cout << PerpendicularChecker2D().check(std::vector<double>{ 1, 0 }, std::vector<double>{ 0, 1 }) << std::endl;
  std::cout << PerpendicularChecker2D().check(std::vector<double>{ 1, 0 }, std::vector<double>{ 1, 1 }) << std::endl;
  std::cout << PerpendicularChecker3D().check(std::vector<double>{ 1, 0, 1 }, std::vector<double>{ 0, 1, 0 }) << std::endl;
  std::cout << PerpendicularChecker3D().check(std::vector<double>{ 1, 0, 1 }, std::vector<double>{ 1, 1, 0 }) << std::endl;
  
  return 0;
}
