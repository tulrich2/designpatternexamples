//
// Created by Tom Ulrich on 24.05.22.
//


#ifndef LUBISINHOUSETRAINING_2_CIRCLECALCULATOR_H
#define LUBISINHOUSETRAINING_2_CIRCLECALCULATOR_H


class IPiCalculator {
public:
  [[nodiscard]] virtual double getPi() const = 0;
};


class CircleCalculator {
public:
  CircleCalculator() = delete;
  explicit CircleCalculator(IPiCalculator *pi_calc);
  
  double getArea(double radius);
  
  
private:
  IPiCalculator *pi_calc_;
};


#endif //LUBISINHOUSETRAINING_2_CIRCLECALCULATOR_H
