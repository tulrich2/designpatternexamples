//
// Created by Tom Ulrich on 24.05.22.
//


#include "CircleCalculator.h"


CircleCalculator::CircleCalculator(IPiCalculator *pi_calc) : pi_calc_(pi_calc) { }

double CircleCalculator::getArea(double radius) {
  return pi_calc_->getPi() * radius * radius;
}
