#include <iostream>
#include "CircleCalculator.h"


class LeibnizPiCalculator : public IPiCalculator {
public:
  LeibnizPiCalculator() = delete;
  explicit LeibnizPiCalculator(int steps) : steps_(steps) { }
  
  [[nodiscard]] double getPi() const override {
    double sum = 0;
    
    int sign = 1;
    for (int i = 0; i < steps_; i++) {
      sum += sign * ((double)1 / ((2 * i) + 1));
      
      sign *= -1;
    }
    
    return 4 * sum;
  }
  
  
private:
  int steps_;
};

class WallisPiCalculator : public IPiCalculator {
public:
  WallisPiCalculator() = delete;
  explicit WallisPiCalculator(int steps) : steps_(steps) { }
  
  [[nodiscard]] double getPi() const override {
    double sum = 1;
    
    for (int i = 1; i <= steps_; i++) {
      sum *= (1 + ((double)1 / (double)((4 * ((long long)i * (long long)i)) - 1)));
    }
    
    return 2 * sum;
  }
  
  
private:
  int steps_;
};


int main(int argc, char **argv) {
  LeibnizPiCalculator pi_calc(1000000);
  CircleCalculator circ_calc(&pi_calc);
  
  std::cout << "A(10 cm) = " << circ_calc.getArea(0.1) << std::endl;
  
  return 0;
}
