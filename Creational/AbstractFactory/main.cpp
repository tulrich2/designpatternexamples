#include <iostream>
#include <sstream>
#include "MensaAccounts.h"


std::string printAccount(IMensaAccount *acc) {
  std::stringstream ss;
  ss << "Name: " << acc->getName();
  ss << ", Balance: " << acc->getBalance();
  ss << ", Multiplier: " << acc->getMultiplier();
  return ss.str();
}


class IAccountFactory {
public:
  virtual ~IAccountFactory() = default;
  virtual IMensaAccount *getAccount(const std::string &name) = 0;
};

class StudentAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new StudentAccount(name);
  }
};

class EmployeeAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new EmployeeAccount(name);
  }
};

class GuestAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new GuestAccount(name);
  }
};


enum class AccountType {
  Student,
  Employee,
  Guest
};


/// Define the interface of the abstract factory


/// Implement the abstract factory


int main(int argc, char **argv) {
  /// Create an account, charge it, and print it to the console
  
  return 0;
}
