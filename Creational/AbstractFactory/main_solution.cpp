#include <iostream>
#include <sstream>
#include "MensaAccounts.h"


std::string printAccount(IMensaAccount *acc) {
  std::stringstream ss;
  ss << "Name: " << acc->getName();
  ss << ", Balance: " << acc->getBalance();
  ss << ", Multiplier: " << acc->getMultiplier();
  return ss.str();
}


class IAccountFactory {
public:
  virtual ~IAccountFactory() = default;
  virtual IMensaAccount *getAccount(const std::string &name) = 0;
};

class StudentAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new StudentAccount(name);
  }
};

class EmployeeAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new EmployeeAccount(name);
  }
};

class GuestAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new GuestAccount(name);
  }
};


enum class AccountType {
  Student,
  Employee,
  Guest
};

class IAbstractAccountFactory {
public:
  virtual ~IAbstractAccountFactory() = default;
  virtual IMensaAccount *getAccount(const std::string &name, AccountType type) = 0;
};

class MensaAccountFactory : public IAbstractAccountFactory {
public:
  IMensaAccount * getAccount(const std::string &name, AccountType type) override {
    IAccountFactory *factory = nullptr;
    
    switch (type) {
      case AccountType::Student:
        factory = new StudentAccountFactory();
        break;
      case AccountType::Employee:
        factory = new EmployeeAccountFactory();
        break;
      case AccountType::Guest:
        factory = new GuestAccountFactory();
        break;
      default:
        return nullptr;
    }
    
    auto new_acc = factory->getAccount(name);
    delete factory;
    return new_acc;
  }
};


int main(int argc, char **argv) {
  MensaAccountFactory factory;
  auto my_account = factory.getAccount("Tom", AccountType::Student);
  my_account->chargeAccount(20.0f);
  std::cout << printAccount(my_account) << std::endl;
  
  return 0;
}
