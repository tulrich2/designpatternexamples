//
// Created by Tom Ulrich on 24.05.22.
//


#include "MensaAccounts.h"


StudentAccount::StudentAccount(std::string name) : name_(std::move(name)), balance_(0.0) { }

const std::string &StudentAccount::getName() const {
  return name_;
}

float StudentAccount::getBalance() const {
  return balance_;
}

float StudentAccount::getMultiplier() const {
  return 1.0f;
}

void StudentAccount::chargeAccount(float amount) {
  balance_ += amount;
}


EmployeeAccount::EmployeeAccount(std::string name) : name_(std::move(name)), balance_(0.0) { }

const std::string &EmployeeAccount::getName() const {
  return name_;
}

float EmployeeAccount::getBalance() const {
  return balance_;
}

float EmployeeAccount::getMultiplier() const {
  return 1.5f;
}

void EmployeeAccount::chargeAccount(float amount) {
  balance_ += amount;
}


GuestAccount::GuestAccount(std::string name) : name_(std::move(name)), balance_(0.0) { }

const std::string &GuestAccount::getName() const {
  return name_;
}

float GuestAccount::getBalance() const {
  return balance_;
}

float GuestAccount::getMultiplier() const {
  return 2.0f;
}

void GuestAccount::chargeAccount(float amount) {
  balance_ += amount;
}
