#include <iostream>
#include <sstream>
#include "MensaAccounts.h"


std::string printAccount(IMensaAccount *acc) {
  std::stringstream ss;
  ss << "Name: " << acc->getName();
  ss << ", Balance: " << acc->getBalance();
  ss << ", Multiplier: " << acc->getMultiplier();
  return ss.str();
}


/// Define the factory interface


/// Implement the concrete factories


int main(int argc, char **argv) {
  /// Create an account, charge it, and print it to the console
  
  return 0;
}
