//
// Created by Tom Ulrich on 24.05.22.
//


#ifndef LUBISINHOUSETRAINING_2_MENSAACCOUNTS_H
#define LUBISINHOUSETRAINING_2_MENSAACCOUNTS_H


#include "IMensaAccount.h"


class StudentAccount : public IMensaAccount {
public:
  StudentAccount() = delete;
  explicit StudentAccount(std::string name);
  
  [[nodiscard]] const std::string & getName() const override;
  [[nodiscard]] float getBalance() const override;
  [[nodiscard]] float getMultiplier() const override;
  void chargeAccount(float amount) override;
  
  
private:
  std::string name_;
  float balance_;
};

class EmployeeAccount : public IMensaAccount {
public:
  EmployeeAccount() = delete;
  explicit EmployeeAccount(std::string name);

  [[nodiscard]] const std::string & getName() const override;
  [[nodiscard]] float getBalance() const override;
  [[nodiscard]] float getMultiplier() const override;
  void chargeAccount(float amount) override;


private:
  std::string name_;
  float balance_;
};

class GuestAccount : public IMensaAccount {
public:
  GuestAccount() = delete;
  explicit GuestAccount(std::string name);

  [[nodiscard]] const std::string & getName() const override;
  [[nodiscard]] float getBalance() const override;
  [[nodiscard]] float getMultiplier() const override;
  void chargeAccount(float amount) override;


private:
  std::string name_;
  float balance_;
};


#endif //LUBISINHOUSETRAINING_2_MENSAACCOUNTS_H
