#include <iostream>
#include <sstream>
#include "MensaAccounts.h"


std::string printAccount(IMensaAccount *acc) {
  std::stringstream ss;
  ss << "Name: " << acc->getName();
  ss << ", Balance: " << acc->getBalance();
  ss << ", Multiplier: " << acc->getMultiplier();
  return ss.str();
}


class IAccountFactory {
public:
  virtual ~IAccountFactory() = default;
  virtual IMensaAccount *getAccount(const std::string &name) = 0;
};

class StudentAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new StudentAccount(name);
  }
};

class EmployeeAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new EmployeeAccount(name);
  }
};

class GuestAccountFactory : public IAccountFactory {
public:
  IMensaAccount *getAccount(const std::string &name) override {
    return new GuestAccount(name);
  }
};


int main(int argc, char **argv) {
  auto student_factory = new StudentAccountFactory();
  auto my_account = student_factory->getAccount("Tom");
  my_account->chargeAccount(20.0f);
  std::cout << printAccount(my_account) << std::endl;
  
  return 0;
}
