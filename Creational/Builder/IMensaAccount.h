//
// Created by Tom Ulrich on 24.05.22.
//


#ifndef LUBISINHOUSETRAINING_2_IMENSAACCOUNT_H
#define LUBISINHOUSETRAINING_2_IMENSAACCOUNT_H


#include <string>


class IMensaAccount {
public:
  [[nodiscard]] virtual const std::string &getName() const = 0;
  [[nodiscard]] virtual float getBalance() const = 0;
  [[nodiscard]] virtual float getMultiplier() const = 0;
  virtual void chargeAccount(float amount) = 0;
};


#endif //LUBISINHOUSETRAINING_2_IMENSAACCOUNT_H
