#include <iostream>
#include <sstream>
#include "MensaAccounts.h"


std::string printAccount(IMensaAccount *acc) {
  std::stringstream ss;
  ss << "Name: " << acc->getName();
  ss << ", Balance: " << acc->getBalance();
  ss << ", Multiplier: " << acc->getMultiplier();
  return ss.str();
}

enum class AccountType {
  Student,
  Employee,
  Guest
};


class IMensaAccountBuilder {
public:
  virtual void setName(const std::string &name) = 0;
  virtual void setInitialBalance(float balance) = 0;
  virtual void setAccountType(AccountType type) = 0;
  [[nodiscard]] virtual IMensaAccount *createAccount() = 0;
};


class MensaAccountBuilder : public IMensaAccountBuilder {
public:
  MensaAccountBuilder() : name_(), initial_balance_(0), acc_type_(AccountType::Student) { }
  
  void setName(const std::string &name) override {
    name_ = name;
  }
  void setInitialBalance(float balance) override {
    initial_balance_ = balance;
  }
  void setAccountType(AccountType type) override {
    acc_type_ = type;
  }
  IMensaAccount *createAccount() override {
    IMensaAccount *new_acc = nullptr;
    
    switch (acc_type_) {
      case AccountType::Student:
        new_acc = new StudentAccount(name_);
        break;
      case AccountType::Employee:
        new_acc = new EmployeeAccount(name_);
        break;
      case AccountType::Guest:
        new_acc = new GuestAccount(name_);
        break;
      default:
        return nullptr;
    }
    
    new_acc->chargeAccount(initial_balance_);
    return new_acc;
  }
  
  
private:
  std::string name_;
  float initial_balance_;
  AccountType acc_type_;
};


int main(int argc, char **argv) {
  MensaAccountBuilder builder;
  builder.setName("Tom");
  builder.setInitialBalance(40.0f);
  builder.setAccountType(AccountType::Student);
  auto my_account = builder.createAccount();
  std::cout << printAccount(my_account) << std::endl;

  return 0;
}
