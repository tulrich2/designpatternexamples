#include <iostream>
#include <string>
#include <vector>
#include "Database.h"


class DataBaseProxy : public IDatabase {
public:
  DataBaseProxy() = delete;
  explicit DataBaseProxy(IDatabase *data_base) : data_base_(data_base), locked(false) { }
  
  void addFile(const std::string &file) override {
    if (locked) {
      throw std::runtime_error("Database was already locked!");
    }
    
    data_base_->addFile(file);
  }
  
  const std::vector<std::string> &getFiles() override {
    return data_base_->getFiles();
  }
  
  void lock() {
    locked = true;
  }
  
  
private:
  IDatabase *data_base_;
  bool locked;
};


int main(int argc, char **argv) {
  std::string input;
  
  auto database = new Database();
  auto proxy = new DataBaseProxy(database);
  
  do {
    std::cin >> input;
    
    if (input == "add") {
      std::string new_file;
      std::cin >> new_file;
      
      try {
        proxy->addFile(new_file);
      } catch (std::runtime_error &ex) {
        std::cout << ex.what() << std::endl;
      }
    } else if (input == "list") {
      for (const auto &file : proxy->getFiles()) {
        std::cout << file << std::endl;
      }
    } else if (input == "do_something") {
      proxy->lock();
      std::cout << "Doing something..." << std::endl;
    } else if (input != "exit") {
      std::cout << "Unknown command '" << input << "'" << std::endl;
    }
  } while (input != "exit");
  
  return 0;
}
