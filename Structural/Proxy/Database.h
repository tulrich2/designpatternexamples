//
// Created by Tom Ulrich on 24.05.22.
//


#ifndef LUBISINHOUSETRAINING_2_DATABASE_H
#define LUBISINHOUSETRAINING_2_DATABASE_H


#include <string>
#include <vector>


class IDatabase {
public:
  virtual void addFile(const std::string &file) = 0;
  [[nodiscard]] virtual const std::vector<std::string> &getFiles() = 0;
};


class Database : public IDatabase {
public:
  void addFile(const std::string &file) override;
  [[nodiscard]] const std::vector<std::string> & getFiles() override;
  
  
private:
  std::vector<std::string> files_;
};


#endif //LUBISINHOUSETRAINING_2_DATABASE_H
