//
// Created by Tom Ulrich on 24.05.22.
//


#include "Database.h"


void Database::addFile(const std::string &file) {
  files_.push_back(file);
}

const std::vector<std::string> &Database::getFiles() {
  return files_;
}
