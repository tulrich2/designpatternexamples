#include <iostream>
#include <string>
#include <vector>
#include "Database.h"


/// Implement proxy for database


int main(int argc, char **argv) {
  std::string input;
  
  auto database = new Database();
  /// Create proxy
  
  do {
    std::cin >> input;
    
    if (input == "add") {
      std::string new_file;
      std::cin >> new_file;
      
      try {
        database->addFile(new_file);
      } catch (std::runtime_error &ex) {
        std::cout << ex.what() << std::endl;
      }
    } else if (input == "list") {
      for (const auto &file : database->getFiles()) {
        std::cout << file << std::endl;
      }
    } else if (input == "do_something") {
      /// Lock database when something was done with the data
      
      std::cout << "Doing something..." << std::endl;
    } else if (input != "exit") {
      std::cout << "Unknown command '" << input << "'" << std::endl;
    }
  } while (input != "exit");
  
  return 0;
}
